# charity-cafe

Problem Description:
It is popular home cafeteria event and a local charity is looking to have a bake sale and second hand
outlet to raise funds for the ones in need. They have promised you a public recognition if you make
software to help run the sale.
There are five edible items they would like to sell on this sale with specific prices and quantities of
each:
Brownie - 65c (48)
Muffin - 1.00 € (36)
Cake Pop - 1.35 € (24)
Apple tart – 1.50 € (60)
Water - 1.50 € (30)
Also they would like to sell toys and clothes, that people donate for sale. Sales list is limited to
following items:
Shirt – 2.00 €
Pants – 3.00 €
Jacket – 4.00 €
Toy – 1.00 €
Quantity of each second hand item in stock is inserted on the day of bake sale.

**The application must calculate the smallest amount of change to give a person if they overpay.
Validations and cash returned functionality works as in every store.**


## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:0a649a920dc54ca7cd97dab64e128717?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:0a649a920dc54ca7cd97dab64e128717?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:0a649a920dc54ca7cd97dab64e128717?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/homework-est/charity-cafe.git
git branch -M main
git push -uf origin main
```


## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

