package com.charity.cafe.product.service.search;

import com.charity.cafe.product.model.ProductDTO;
import com.charity.cafe.product.model.ProductSearchDTO;
import org.springframework.data.domain.Page;

public interface ProductSearchService {
  Page<ProductDTO> search(ProductSearchDTO productSearch);
}
