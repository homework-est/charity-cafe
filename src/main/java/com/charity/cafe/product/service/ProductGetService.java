package com.charity.cafe.product.service;

import com.charity.cafe.product.model.Product;
import com.charity.cafe.product.model.ProductDTO;
import com.charity.cafe.product.model.ProductSearchDTO;
import org.springframework.data.domain.Page;

public interface ProductGetService {
  Page<ProductDTO> search(ProductSearchDTO productSearch);
  ProductDTO getById(Long id);
  Product findById(Long id);
}
