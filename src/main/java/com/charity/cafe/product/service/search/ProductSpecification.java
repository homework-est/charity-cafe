package com.charity.cafe.product.service.search;

import static com.charity.cafe.util.Constant.SEARCH_CRITERIA_CATEGORY;

import com.charity.cafe.product.model.Product;
import javax.persistence.criteria.Join;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

@AllArgsConstructor
public class ProductSpecification {

    public static Specification<Product> getCategoryByLongPropertySpec(String property, Long longValue) {
        return (root, query, criteriaBuilder) -> {
            final Join<Object, Object> categoryJoin = root.join(SEARCH_CRITERIA_CATEGORY);
            return criteriaBuilder.equal(categoryJoin.get(property).as(Long.class), longValue);
        };
    }
}

