package com.charity.cafe.product.service.search;

import static com.charity.cafe.util.Constant.SEARCH_CRITERIA_ID;
import static com.charity.cafe.util.Constant.SORT_TYPE_ASCENDING;

import com.charity.cafe.product.model.Product;
import com.charity.cafe.product.model.ProductDTO;
import com.charity.cafe.product.model.ProductSearchDTO;
import com.charity.cafe.product.repository.ProductRepository;
import com.charity.cafe.util.DenominationUtil;
import com.charity.cafe.util.SearchSpecification;
import java.util.Objects;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductSearchServiceImpl implements ProductSearchService {

  private final ProductRepository productRepository;
  private final SearchSpecification<Product> searchSpecification;
  private final ModelMapper modelMapper;

  @Override
  public Page<ProductDTO> search(ProductSearchDTO productSearch) {
    final Pageable page = constructPageRequest(productSearch);
    return paginateProducts(page, productSearch)
          .map(product -> {
                            ProductDTO productDTO = modelMapper.map(product, ProductDTO.class);
                            //todo: it should have separate custom mapping method
                            productDTO.setPrice(DenominationUtil.convertCentsToEuros(product.getPrice()));
                            return productDTO;
                          });
  }

  private Pageable constructPageRequest(ProductSearchDTO productSearch) {
    return PageRequest.of(productSearch.getPageNumber(), productSearch.getPageSize(), getSortBy(productSearch));
  }

  private Sort getSortBy(ProductSearchDTO productSearch) {
    if(productSearch.getSortDirection().equalsIgnoreCase(SORT_TYPE_ASCENDING)) {
      return Sort.by(productSearch.getSortBy()).ascending();
    } else {
      return Sort.by(productSearch.getSortBy()).descending();
    }
  }

  private Page<Product> paginateProducts(Pageable pageable,ProductSearchDTO productSearch) {
    Specification<Product> specifications = buildSpecifications(productSearch);
    return productRepository.findAll(specifications, pageable);
  }

  private Specification<Product> buildSpecifications(ProductSearchDTO productSearch) {
    Specification<Product> specifications = Specification.where(searchSpecification.getEntityByIdSpec());

    if(!Objects.isNull(productSearch.getCategoryId()) && productSearch.getCategoryId() > 0l) {
      specifications = specifications.and(ProductSpecification.getCategoryByLongPropertySpec(SEARCH_CRITERIA_ID, productSearch.getCategoryId()));
    }

    return specifications;
  }
}
