package com.charity.cafe.product.service;

import com.charity.cafe.product.model.Product;
import com.charity.cafe.product.model.ProductDTO;
import com.charity.cafe.product.model.ProductSearchDTO;
import com.charity.cafe.product.repository.ProductRepository;
import com.charity.cafe.product.service.search.ProductSearchService;
import com.charity.cafe.util.DenominationUtil;
import javax.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductGetServiceImpl implements ProductGetService {

  private final ProductSearchService productSearchService;
  private final ProductRepository productRepository;
  private final ModelMapper modelMapper;

  @Override
  public Page<ProductDTO> search(ProductSearchDTO productSearch) {
    return productSearchService.search(productSearch);
  }

  @Override
  public ProductDTO getById(Long id) {
    final Product product = findById(id);
    final ProductDTO productDTO = modelMapper.map(product, ProductDTO.class);
    //todo: it should have separate custom mapping method
    productDTO.setPrice(DenominationUtil.convertCentsToEuros(product.getPrice()));
    return productDTO;
  }

  @Override
  public Product findById(Long id) {
    return productRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Product not found - id:" + id));
  }
}
