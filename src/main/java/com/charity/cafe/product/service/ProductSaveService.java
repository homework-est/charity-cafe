package com.charity.cafe.product.service;

import com.charity.cafe.product.model.ProductDTO;

public interface ProductSaveService {
  ProductDTO save(ProductDTO product);
}
