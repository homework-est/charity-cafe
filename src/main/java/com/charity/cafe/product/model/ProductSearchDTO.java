package com.charity.cafe.product.model;

import lombok.Data;

@Data
public class ProductSearchDTO {

  private Long categoryId;

  private int pageSize;
  private int pageNumber;
  private String sortBy;
  private String sortDirection;
}
