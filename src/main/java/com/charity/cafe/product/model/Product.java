package com.charity.cafe.product.model;

import com.charity.cafe.category.model.Category;
import com.charity.cafe.core.model.Auditable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Where;

@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString
@Where(clause = "deleted_at IS NULL")
public class Product extends Auditable<String> {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String name;
  private int quantity;
  private Long price;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "category_id", unique = true)
  private Category category;
}
