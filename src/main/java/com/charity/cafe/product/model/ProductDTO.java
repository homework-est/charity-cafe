package com.charity.cafe.product.model;

import com.charity.cafe.category.model.CategoryDTO;
import java.math.BigDecimal;
import lombok.Data;

@Data
public class ProductDTO {
  private Long id;
  private String name;
  private int quantity;
  private BigDecimal price;
  private CategoryDTO category;
}
