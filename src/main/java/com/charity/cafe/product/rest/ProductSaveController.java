package com.charity.cafe.product.rest;

import com.charity.cafe.product.model.ProductDTO;
import com.charity.cafe.product.service.ProductSaveService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/product")
@RequiredArgsConstructor
public class ProductSaveController {

  private final ProductSaveService productSaveService;

  @PostMapping("/save")
  public ResponseEntity save(@RequestBody ProductDTO productDTO) {
    productDTO = productSaveService.save(productDTO);
    return ResponseEntity.ok(productDTO);
  }
}
