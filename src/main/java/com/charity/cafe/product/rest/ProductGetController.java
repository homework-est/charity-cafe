package com.charity.cafe.product.rest;

import com.charity.cafe.product.model.ProductDTO;
import com.charity.cafe.product.model.ProductSearchDTO;
import com.charity.cafe.product.service.ProductGetService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/product")
@RequiredArgsConstructor
public class ProductGetController {

  private final ProductGetService productGetService;

  @GetMapping("/search")
  public ResponseEntity search(ProductSearchDTO productSearch) {
    final Page<ProductDTO> products = productGetService.search(productSearch);
    return ResponseEntity.ok(products);
  }

  @GetMapping("/{id}")
  public ResponseEntity getProduct(@PathVariable Long productId) {
    final ProductDTO product = productGetService.getById(productId);
    return ResponseEntity.ok(product);
  }
}
