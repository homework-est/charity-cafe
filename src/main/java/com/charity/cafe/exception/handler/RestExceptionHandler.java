package com.charity.cafe.exception.handler;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_ACCEPTABLE;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

import com.charity.cafe.exception.model.ErrorCode;
import com.charity.cafe.exception.model.RestErrorMessageModel;
import com.charity.cafe.exception.service.ApplicationErrorService;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler implements AccessDeniedHandler {

  @ExceptionHandler(EntityNotFoundException.class)
  protected ResponseEntity<Object> handleEntityNotFound(EntityNotFoundException enfException) {
    saveException(ErrorCode.ENTITY_NOT_FOUND, enfException);
    return new ResponseEntity<>(new RestErrorMessageModel(ErrorCode.ENTITY_NOT_FOUND, enfException.getMessage()), BAD_REQUEST);
  }

  @ExceptionHandler(AuthenticationException.class)
  protected ResponseEntity<Object> handleAccessAuthentication(AuthenticationException authenticationException) {
    saveException(ErrorCode.ACCESS_DENIED, authenticationException);
    return new ResponseEntity<>(new RestErrorMessageModel(ErrorCode.ACCESS_DENIED, authenticationException.getMessage()), UNAUTHORIZED);
  }

  @ExceptionHandler(AccessDeniedException.class)
  protected ResponseEntity<Object> handleAccessDeniedException(AccessDeniedException accessDeniedException) {
    saveException(ErrorCode.ACCESS_DENIED, accessDeniedException);
    return new ResponseEntity<>(new RestErrorMessageModel(ErrorCode.ACCESS_DENIED, accessDeniedException.getMessage()), FORBIDDEN);
  }

  @ExceptionHandler(UnsupportedEncodingException.class)
  protected ResponseEntity<Object> handleUnsupportedEncodingException(UnsupportedEncodingException unsupportedEncodingException) {
    saveException(ErrorCode.SERVER_RUNTIME_ERROR, unsupportedEncodingException);
    return new ResponseEntity<>(new RestErrorMessageModel(ErrorCode.SERVER_RUNTIME_ERROR, unsupportedEncodingException.getMessage()), NOT_ACCEPTABLE);
  }

  @ExceptionHandler({IOException.class, InterruptedException.class, Exception.class})
  protected ResponseEntity<Object> handleIOException(Exception exception) {
    saveException(ErrorCode.SERVER_RUNTIME_ERROR, exception);
    return new ResponseEntity<>(new RestErrorMessageModel(ErrorCode.SERVER_RUNTIME_ERROR, exception.getMessage()), INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(LockedException.class)
  protected ResponseEntity<Object> handleLockedException(LockedException lockedException) {
    saveException(ErrorCode.ACCESS_DENIED, lockedException);
    return new ResponseEntity<>(new RestErrorMessageModel(ErrorCode.ACCESS_DENIED, lockedException.getMessage()), UNAUTHORIZED);
  }

  @ExceptionHandler(IllegalArgumentException.class)
  protected ResponseEntity<Object> handleIllegalArgumentException(IllegalArgumentException illegalArgumentException) {
    saveException(ErrorCode.SERVER_RUNTIME_ERROR, illegalArgumentException);
    return new ResponseEntity<>(new RestErrorMessageModel(ErrorCode.SERVER_RUNTIME_ERROR, illegalArgumentException.getMessage()), BAD_REQUEST);
  }

  @ExceptionHandler(RuntimeException.class)
  protected ResponseEntity<Object> handleDefaultException(RuntimeException runtimeException) {
    saveException(ErrorCode.SERVER_RUNTIME_ERROR, runtimeException);
    return new ResponseEntity<>(new RestErrorMessageModel(ErrorCode.SERVER_RUNTIME_ERROR, runtimeException.getMessage()), INTERNAL_SERVER_ERROR);
  }

  @Override
  protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
                                                                HttpHeaders headers,
                                                                HttpStatus status,
                                                                WebRequest request) {
    return new ResponseEntity<>(new RestErrorMessageModel(ErrorCode.SERVER_RUNTIME_ERROR, ex.getMessage()), BAD_REQUEST);
  }

  @Override
  public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) {
    response.setStatus(HttpStatus.FORBIDDEN.value());
  }

  private void saveException(ErrorCode errorCode, Exception exception) {
    ApplicationErrorService.save(errorCode, exception);
  }
}
