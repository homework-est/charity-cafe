package com.charity.cafe.exception.model;

public enum ErrorCode {
    SERVER_RUNTIME_ERROR,
    ACCESS_DENIED,
    ENTITY_NOT_FOUND,
}
