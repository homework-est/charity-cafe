package com.charity.cafe.exception;

public class PriceMismatchException extends RuntimeException {
  public PriceMismatchException(String message) {
    super(message);
  }

  public PriceMismatchException(String message, Throwable cause) {
    super(message, cause);
  }
}
