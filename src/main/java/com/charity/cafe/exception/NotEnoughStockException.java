package com.charity.cafe.exception;

public class NotEnoughStockException extends RuntimeException {
  public NotEnoughStockException(String message) {
    super(message);
  }

  public NotEnoughStockException(String message, Throwable cause) {
    super(message, cause);
  }
}
