package com.charity.cafe.exception.repository;

import com.charity.cafe.exception.model.ApplicationError;
import org.springframework.data.repository.CrudRepository;

public interface ApplicationErrorRepository extends CrudRepository<ApplicationError, Long> {

}
