package com.charity.cafe.exception.service;

import static com.charity.cafe.util.Constant.MESSAGE_UTILITY_CLASS;

import com.charity.cafe.exception.model.ApplicationError;
import com.charity.cafe.exception.model.ErrorCode;
import com.charity.cafe.exception.repository.ApplicationErrorRepository;

import com.charity.cafe.util.BeanUtil;
import java.io.PrintWriter;
import java.io.StringWriter;

public class ApplicationErrorService {

  private ApplicationErrorService() {
    throw new IllegalStateException(MESSAGE_UTILITY_CLASS);
  }

  public static void save(ErrorCode errorCode, Exception exception) {
    ApplicationError applicationError = buildApplicationError(errorCode, exception);
    ApplicationErrorRepository applicationErrorRepository = BeanUtil.getBean(ApplicationErrorRepository.class);
    applicationErrorRepository.save(applicationError);
  }

  private static ApplicationError buildApplicationError(ErrorCode errorCode, Exception exception) {
    StringWriter errors = new StringWriter();
    exception.printStackTrace(new PrintWriter(errors));

    return ApplicationError
        .builder()
        .errorCode(errorCode)
        .message(exception.getMessage())
        .errorLog(errors.toString())
        .build();
  }
}
