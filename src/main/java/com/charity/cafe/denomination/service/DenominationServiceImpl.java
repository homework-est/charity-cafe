package com.charity.cafe.denomination.service;

import static java.util.Collections.emptyList;

import com.charity.cafe.denomination.model.Denomination;
import com.charity.cafe.denomination.model.DenominationDTO;
import com.charity.cafe.denomination.repository.DenominationRepository;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DenominationServiceImpl implements DenominationService {

  private final DenominationRepository denominationRepository;
  private final ModelMapper modelMapper;

  @Override
  public List<DenominationDTO> getAll() {
    final List<Denomination> denominations = findAll();
    final Type listType = new TypeToken<List<DenominationDTO>>() {}.getType();
    return modelMapper.map(Optional.ofNullable(denominations).orElse(emptyList()), listType);
  }

  private List<Denomination> findAll() {
    return denominationRepository.findAll();
  }
}
