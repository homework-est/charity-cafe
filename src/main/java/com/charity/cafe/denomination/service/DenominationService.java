package com.charity.cafe.denomination.service;

import com.charity.cafe.denomination.model.DenominationDTO;
import java.util.List;

public interface DenominationService {
  List<DenominationDTO> getAll();
}
