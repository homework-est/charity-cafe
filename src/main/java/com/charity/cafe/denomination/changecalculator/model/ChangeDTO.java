package com.charity.cafe.denomination.changecalculator.model;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ChangeDTO {
  private List<ChangeDenominationDTO> changeDenominations;
}
