package com.charity.cafe.denomination.changecalculator;


import static org.springframework.util.CollectionUtils.isEmpty;

import com.charity.cafe.denomination.changecalculator.model.ChangeDTO;
import com.charity.cafe.denomination.model.DenominationDTO;
import java.util.ArrayList;
import java.util.List;


public class ChangeHandler {
  private ChangeMaker highestDenominationChangeMaker;


  public void prepareChainOfChangeMakers(List<DenominationDTO> denominations) {
    if(isEmpty(denominations)) {
      throw new RuntimeException("Denominations are not present");
    }

    // In loop it assigns next changeMaker for currentChangeMaker. (Starting from cent 1 to Note 500)
    ChangeMaker changeMaker = null;
    for (DenominationDTO denomination : denominations) {
      final ChangeMaker currentChangeMaker = new ChangeMaker();
      currentChangeMaker.setDenomination(denomination);
      if(changeMaker != null) {
        currentChangeMaker.setNextChangeMaker(changeMaker);
      }
      changeMaker = currentChangeMaker;
    }

    highestDenominationChangeMaker = changeMaker;
  }


  public ChangeDTO getChanges(Long dueAmount) {
    final ChangeDTO changeDTO = new ChangeDTO(new ArrayList<>());
    // it starts calculating change from 500 note
    highestDenominationChangeMaker.calculateChange(dueAmount, changeDTO);
    return changeDTO;
  }
}
