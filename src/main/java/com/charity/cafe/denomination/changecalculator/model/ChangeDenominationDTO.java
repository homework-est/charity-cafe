package com.charity.cafe.denomination.changecalculator.model;

import com.charity.cafe.denomination.model.DenominationDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChangeDenominationDTO {
  private DenominationDTO denomination;
  private int quantity;
  private Long total;
}
