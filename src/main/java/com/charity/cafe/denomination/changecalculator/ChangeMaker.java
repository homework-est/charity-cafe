package com.charity.cafe.denomination.changecalculator;


import com.charity.cafe.denomination.changecalculator.model.ChangeDTO;
import com.charity.cafe.denomination.changecalculator.model.ChangeDenominationDTO;
import com.charity.cafe.denomination.model.DenominationDTO;
import lombok.Data;

@Data
public class ChangeMaker {
  private DenominationDTO denomination;
  private ChangeMaker nextChangeMaker;

  public void calculateChange(Long dueAmount, ChangeDTO changeDTO) {
    int quantity = (int) Math.floor(dueAmount / denomination.getValue());
    Long changeTotal = denomination.getValue() * quantity;
    if (quantity > 0) {
      addDenominationChange(changeDTO, quantity, changeTotal);
      dueAmount -= changeTotal;
    }

    if (dueAmount > 0) {
      if (nextChangeMaker == null) {
        throw new RuntimeException("There are errors in calculating changes: next change maker is not available");
      }
      nextChangeMaker.calculateChange(dueAmount, changeDTO);
    }
  }

  private void addDenominationChange(ChangeDTO changeDTO, int quantity, Long changeTotal) {
    changeDTO.getChangeDenominations().add(ChangeDenominationDTO
                                            .builder()
                                            .denomination(denomination)
                                            .quantity(quantity)
                                            .total(changeTotal)
                                            .build());
  }
}
