package com.charity.cafe.denomination.model;

import com.charity.cafe.denomination.model.Denomination.Type;
import lombok.Data;

@Data
public class DenominationDTO {
  private Long id;
  private String name;
  private Type type;
  private Long value;
}
