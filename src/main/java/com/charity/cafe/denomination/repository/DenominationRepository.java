package com.charity.cafe.denomination.repository;

import com.charity.cafe.denomination.model.Denomination;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DenominationRepository extends JpaRepository<Denomination, Long> {
}
