package com.charity.cafe.order.rest;

import com.charity.cafe.order.model.OrderDTO;
import com.charity.cafe.order.service.OrderGetService;
import java.math.BigDecimal;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/order")
@RequiredArgsConstructor
public class OrderGetController {

  private final OrderGetService orderGetService;

  @GetMapping("/calculate-total")
  public ResponseEntity calculateTotal(@RequestBody @Valid OrderDTO orderDTO) {
    BigDecimal total = orderGetService.calculateTotal(orderDTO);
    return ResponseEntity.ok(total);
  }

  @GetMapping("/{id}")
  public ResponseEntity getOrder(@PathVariable Long orderId) {
    //todo: needs improvement so CartProductDTO doesn't return long values
    final OrderDTO order = orderGetService.getById(orderId);
    return ResponseEntity.ok(order);
  }
}
