package com.charity.cafe.order.rest;

import com.charity.cafe.order.model.OrderCheckoutDTO;
import com.charity.cafe.order.model.OrderCheckoutResultDTO;
import com.charity.cafe.order.service.OrderCheckoutService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/order")
@RequiredArgsConstructor
public class OrderCheckoutController {

  private final OrderCheckoutService orderCheckoutService;

  @PostMapping("/checkout")
  public ResponseEntity checkout(@RequestBody OrderCheckoutDTO orderCheckoutDTO) {
    final OrderCheckoutResultDTO orderCheckoutResultDTO = orderCheckoutService.checkout(orderCheckoutDTO);
    return ResponseEntity.ok(orderCheckoutResultDTO);
  }
}
