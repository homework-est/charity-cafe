package com.charity.cafe.order.service;

import static com.charity.cafe.util.DenominationUtil.convertCentsToEuros;

import com.charity.cafe.exception.NotEnoughStockException;
import com.charity.cafe.order.model.CartProductDTO;
import com.charity.cafe.order.model.Order;
import com.charity.cafe.order.model.OrderDTO;
import com.charity.cafe.order.repository.OrderRepository;
import com.charity.cafe.product.model.Product;
import com.charity.cafe.product.service.ProductGetService;

import com.charity.cafe.util.DenominationUtil;
import java.math.BigDecimal;
import javax.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderGetServiceImpl implements OrderGetService {

  private final OrderRepository orderRepository;
  private final ProductGetService productGetService;
  private final ModelMapper modelMapper;

  @Override
  public BigDecimal calculateTotal(OrderDTO orderDTO) {
    Long total = 0L;
    for (CartProductDTO cartProduct : orderDTO.getCartProducts()) {
          Long cartProductTotal = getTotalProductPrice(cartProduct);
          total += cartProductTotal;
    }
    return convertCentsToEuros(total);
  }

  @Override
  public OrderDTO getById(Long id) {
    final Order order = findById(id);
    final OrderDTO orderDTO = modelMapper.map(order, OrderDTO.class);
    //todo: it should have separate custom mapping method
    orderDTO.setTotal(DenominationUtil.convertCentsToEuros(order.getTotal()));
    return orderDTO;
  }

  private Long getTotalProductPrice(CartProductDTO cartProduct) {
    final Product product = productGetService.findById(cartProduct.getProduct().getId());
    if (product.getQuantity() < cartProduct.getQuantity()) {
      throw new NotEnoughStockException("Not enough Quantity present for product" + product.getName());
    }
    return product.getPrice() * cartProduct.getQuantity();
  }

  private Order findById(Long id) {
    return orderRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Order not found - id:" + id));
  }
}
