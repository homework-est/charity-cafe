package com.charity.cafe.order.service;

import static com.charity.cafe.util.DenominationUtil.getLongValue;

import com.charity.cafe.exception.PriceMismatchException;
import com.charity.cafe.order.model.CheckoutChangeDenominationDTO;
import com.charity.cafe.order.model.OrderCheckoutDTO;
import com.charity.cafe.order.model.OrderCheckoutResultDTO;
import com.charity.cafe.order.model.OrderDTO;
import java.math.BigDecimal;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Service
@RequiredArgsConstructor
public class OrderCheckoutServiceImpl implements OrderCheckoutService {

  private final OrderSaveService orderSaveService;
  private final OrderGetService orderGetService;
  private final OrderCheckoutChangeService orderCheckoutChangeService;

  @Override
  public OrderCheckoutResultDTO checkout(@RequestBody @Valid OrderCheckoutDTO orderCheckoutDTO) {
    verifyCartTotal(orderCheckoutDTO.getOrderDTO());

    OrderDTO orderDTO = orderSaveService.save(orderCheckoutDTO.getOrderDTO());
    final List<CheckoutChangeDenominationDTO> checkoutChangeDenominations = orderCheckoutChangeService.getChanges(getLongValue(orderCheckoutDTO.getPaidAmount()));

    return OrderCheckoutResultDTO
          .builder()
          .orderDTO(orderDTO)
          .checkoutChangeDenomination(checkoutChangeDenominations)
          .build();
  }

  private void verifyCartTotal(OrderDTO orderDTO) {
    final BigDecimal cartTotal = orderGetService.calculateTotal(orderDTO);
    if (cartTotal != orderDTO.getTotal()) {
      throw new PriceMismatchException("Cart Totals do not match! Please try adding products one more time.");
    }
  }
}
