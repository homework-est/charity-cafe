package com.charity.cafe.order.service;

import com.charity.cafe.order.model.OrderDTO;
import java.math.BigDecimal;

public interface OrderGetService {
  BigDecimal calculateTotal(OrderDTO orderDTO);
  OrderDTO getById(Long id);
}
