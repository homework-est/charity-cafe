package com.charity.cafe.order.service;

import com.charity.cafe.order.model.Order;
import com.charity.cafe.order.model.OrderDTO;
import com.charity.cafe.order.repository.OrderRepository;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderSaveServiceImpl implements OrderSaveService {

  private final OrderRepository orderRepository;
  private final ModelMapper modelMapper;

  @Override
  public OrderDTO save(OrderDTO orderDTO) {
    final Order order = modelMapper.map(orderDTO, Order.class);
    Optional.ofNullable(order.getCartProducts())
            .ifPresent(cartProducts -> cartProducts.forEach(cartProduct -> cartProduct.setOrder(order)));
    final Order dbOrder = save(order);
    return modelMapper.map(dbOrder, OrderDTO.class);
  }

  private Order save(Order order) {
    return orderRepository.save(order);
  }
}
