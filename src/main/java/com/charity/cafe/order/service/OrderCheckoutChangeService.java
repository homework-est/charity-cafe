package com.charity.cafe.order.service;


import com.charity.cafe.order.model.CheckoutChangeDenominationDTO;
import java.util.List;

public interface OrderCheckoutChangeService {
  List<CheckoutChangeDenominationDTO> getChanges(Long paidAmount);
}
