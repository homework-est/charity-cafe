package com.charity.cafe.order.service;

import com.charity.cafe.order.model.OrderCheckoutDTO;
import com.charity.cafe.order.model.OrderCheckoutResultDTO;

public interface OrderCheckoutService {
  OrderCheckoutResultDTO checkout(OrderCheckoutDTO orderCheckoutDTO);
}
