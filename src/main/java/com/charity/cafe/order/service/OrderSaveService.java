package com.charity.cafe.order.service;

import com.charity.cafe.order.model.OrderDTO;

public interface OrderSaveService {
  OrderDTO save(OrderDTO order);
}
