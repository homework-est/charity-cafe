package com.charity.cafe.order.service;

import static com.charity.cafe.util.DenominationUtil.convertCentsToEuros;
import static com.charity.cafe.util.Utils.streamOf;

import com.charity.cafe.denomination.changecalculator.ChangeHandler;
import com.charity.cafe.denomination.changecalculator.model.ChangeDTO;
import com.charity.cafe.denomination.changecalculator.model.ChangeDenominationDTO;
import com.charity.cafe.denomination.model.DenominationDTO;
import com.charity.cafe.denomination.service.DenominationService;
import com.charity.cafe.order.model.CheckoutChangeDenominationDTO;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderCheckoutChangeServiceImpl implements OrderCheckoutChangeService {

  private final DenominationService denominationService;

  @Override
  public List<CheckoutChangeDenominationDTO> getChanges(Long paidAmount) {
    final List<DenominationDTO> denominations = denominationService.getAll();

    final ChangeHandler changeHandler = new ChangeHandler();
    changeHandler.prepareChainOfChangeMakers(denominations);
    final ChangeDTO changeDTO = changeHandler.getChanges(paidAmount);

    return mapToCheckoutChangeDenominations(changeDTO.getChangeDenominations());
  }

  private List<CheckoutChangeDenominationDTO> mapToCheckoutChangeDenominations(List<ChangeDenominationDTO> changeDenominations) {
    final List<CheckoutChangeDenominationDTO> checkoutChangeDenominations = new ArrayList<>();

    streamOf(changeDenominations).forEach(changeDenomination -> {
      CheckoutChangeDenominationDTO checkoutChangeDenomination = new CheckoutChangeDenominationDTO();

      checkoutChangeDenomination.setName(changeDenomination.getDenomination().getName());
      checkoutChangeDenomination.setValue(convertCentsToEuros(changeDenomination.getDenomination().getValue()));
      checkoutChangeDenomination.setQuantity(changeDenomination.getQuantity());
      checkoutChangeDenomination.setTotal(convertCentsToEuros(changeDenomination.getTotal()));

      checkoutChangeDenominations.add(checkoutChangeDenomination);
    });

    return checkoutChangeDenominations;
  }
}
