package com.charity.cafe.order.model;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class CheckoutChangeDenominationDTO {
  private String name;
  private BigDecimal value;
  private int quantity;
  private BigDecimal total;
}
