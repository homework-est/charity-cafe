package com.charity.cafe.order.model;

import com.charity.cafe.core.model.Auditable;
import com.fasterxml.jackson.annotation.JsonBackReference;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Where;

@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString
@Where(clause = "deleted_at IS NULL")
public class Order extends Auditable<String> {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String status;
  private Long total;

  @OneToMany(mappedBy = "order", orphanRemoval = true, cascade = CascadeType.MERGE)
  @JsonBackReference
  private List<CartProduct> cartProducts;
}
