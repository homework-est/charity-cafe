package com.charity.cafe.order.model;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderCheckoutResultDTO {
  private OrderDTO orderDTO;
  private List<CheckoutChangeDenominationDTO> checkoutChangeDenomination;
}
