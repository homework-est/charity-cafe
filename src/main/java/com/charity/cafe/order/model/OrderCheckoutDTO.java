package com.charity.cafe.order.model;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class OrderCheckoutDTO {
  private OrderDTO orderDTO;
  BigDecimal paidAmount;
}
