package com.charity.cafe.order.model;

import com.charity.cafe.core.model.Auditable;
import com.charity.cafe.product.model.Product;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Where;

@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString
@Where(clause = "deleted_at IS NULL")
public class CartProduct extends Auditable<String> {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String name;
  private int quantity;
  private Long price;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "order_id")
  @ToString.Exclude
  private Order order;

  @OneToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "product_id")
  @ToString.Exclude
  private Product product;
}
