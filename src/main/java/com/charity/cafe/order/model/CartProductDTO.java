package com.charity.cafe.order.model;

import com.charity.cafe.product.model.ProductDTO;
import lombok.Data;

@Data
public class CartProductDTO {
  private Long id;
  private String name;
  private int quantity;
  private Long price;

  private ProductDTO product;
}
