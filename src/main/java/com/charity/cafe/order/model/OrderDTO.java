package com.charity.cafe.order.model;

import java.math.BigDecimal;
import java.util.List;
import javax.validation.constraints.NotEmpty;
import lombok.Data;

@Data
public class OrderDTO {
  private Long id;
  private String status;
  private BigDecimal total;

  @NotEmpty(message="Your cart is empty")
  private List<CartProductDTO> cartProducts;
}
