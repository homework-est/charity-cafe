package com.charity.cafe.category.model;

import lombok.Data;

@Data
public class CategoryDTO {
  private Long id;
  private String name;
}
