package com.charity.cafe.category.service;

import com.charity.cafe.category.model.CategoryDTO;
import java.util.List;

public interface CategoryService {
  List<CategoryDTO> getAll();
}
