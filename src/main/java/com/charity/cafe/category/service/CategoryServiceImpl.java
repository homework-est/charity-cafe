package com.charity.cafe.category.service;

import static java.util.Collections.emptyList;

import com.charity.cafe.category.model.Category;
import com.charity.cafe.category.model.CategoryDTO;
import com.charity.cafe.category.repository.CategoryRepository;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {

  private final CategoryRepository categoryRepository;
  private final ModelMapper modelMapper;

  @Override
  public List<CategoryDTO> getAll() {
    final List<Category> categories = categoryRepository.findAll();
    final Type listType = new TypeToken<List<CategoryDTO>>() {}.getType();
    return modelMapper.map(Optional.ofNullable(categories).orElse(emptyList()), listType);
  }
}
