package com.charity.cafe.category.rest;

import com.charity.cafe.category.model.CategoryDTO;
import com.charity.cafe.category.service.CategoryService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/category")
@RequiredArgsConstructor
public class CategoryController {

  private final CategoryService categoryService;

  @GetMapping("/all")
  public ResponseEntity getAll() {
    final List<CategoryDTO> categories = categoryService.getAll();
    return ResponseEntity.ok(categories);
  }
}
