package com.charity.cafe.util;

public class Constant {

  private Constant() {
    throw new IllegalStateException(MESSAGE_UTILITY_CLASS);
  }

  public static final String MESSAGE_UTILITY_CLASS = "Utility class";

  public static final String SEARCH_CRITERIA_ID = "id";
  public static final String SEARCH_CRITERIA_CATEGORY = "category";


  public static final String SORT_TYPE_ASCENDING = "ASC";
}
