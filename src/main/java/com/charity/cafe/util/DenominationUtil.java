package com.charity.cafe.util;

import java.math.BigDecimal;

public interface DenominationUtil {
  static BigDecimal convertCentsToEuros(Long cents) {
    BigDecimal bigDecimalCents = BigDecimal.valueOf(cents);
    BigDecimal bigDecimalEuros = bigDecimalCents.divide(BigDecimal.valueOf(100));
    return bigDecimalEuros;
  }

  static Long getLongValue(BigDecimal value) {
    return value.multiply(BigDecimal.valueOf(100)).longValue();
  }
}
