package com.charity.cafe.util;

import static java.util.Collections.emptyList;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public interface Utils {
  static <T> Stream<T> streamOf(List<T> list) {
    return Optional.ofNullable(list).orElse(emptyList()).stream();
  }
}
