package com.charity.cafe.util;

import static com.charity.cafe.util.Constant.SEARCH_CRITERIA_ID;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
public class SearchSpecification<T> {

    public Specification<T> getEntityByIdSpec() {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.greaterThan(root.get(SEARCH_CRITERIA_ID), 0);
    }
}
