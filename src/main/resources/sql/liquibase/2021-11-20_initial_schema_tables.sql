--liquibase formatted sqlFile
--changeset bhaveshkhatri:Database create privileges splitStatements:true

-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: charity_cafe
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Food','2021-11-20 08:32:04','bhavesh','2021-11-20 08:32:04','bhavesh'),(2,'Clothes','2021-11-20 08:32:04','bhavesh','2021-11-20 08:32:04','bhavesh'),(3,'Toys','2021-11-20 08:32:04','bhavesh','2021-11-20 08:32:04','bhavesh');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `denomination`
--

DROP TABLE IF EXISTS `denomination`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `denomination` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `value` bigint NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `denomination`
--

LOCK TABLES `denomination` WRITE;
/*!40000 ALTER TABLE `denomination` DISABLE KEYS */;
INSERT INTO `denomination` VALUES (1,'1 Cent','COIN',1),(2,'2 Cent','COIN',2),(3,'5 Cent','COIN',5),(4,'10 Cent','COIN',10),(5,'20 Cent','COIN',20),(6,'50 Cent','COIN',50),(7,'1 Euro','COIN',100),(8,'2 Euro','COIN',200),(9,'5 Euro','NOTE',500),(10,'10 Euro','NOTE',1000),(11,'20 Euro','NOTE',2000),(12,'50 Euro','NOTE',5000),(13,'100 Euro','NOTE',10000),(14,'200 Euro','NOTE',20000),(15,'500 Euro','NOTE',50000);
/*!40000 ALTER TABLE `denomination` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `cash_available`
--

DROP TABLE IF EXISTS `cash_available`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cash_available` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `denomination_id` bigint NOT NULL,
  `quantity` bigint DEFAULT '0',
  `value` bigint DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `denomination_id_denomination_cash_avaialble_idx` (`denomination_id`),
  CONSTRAINT `denomination_id_denomination_cash_avaialble` FOREIGN KEY (`denomination_id`) REFERENCES `denomination` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cash_available`
--

LOCK TABLES `cash_available` WRITE;
/*!40000 ALTER TABLE `cash_available` DISABLE KEYS */;
INSERT INTO `cash_available` VALUES (1,1,100,100),(2,2,100,200),(3,3,100,500),(4,4,100,1000),(5,5,100,2000),(6,6,100,5000),(7,7,100,10000),(8,8,100,20000),(9,9,50,25000),(10,10,50,50000),(11,11,50,100000),(12,12,10,50000),(13,13,5,50000),(14,14,5,100000),(15,15,0,0);
/*!40000 ALTER TABLE `cash_available` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `category_id` bigint NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `quantity` int DEFAULT '0',
  `price` bigint DEFAULT '0',
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id_category_product_idx` (`category_id`),
  CONSTRAINT `category_id_category_product` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,1,'Brownie',48,65,'2021-11-20 08:32:04','bhavesh','2021-11-20 08:32:04','bhavesh'),(2,1,'Muffin',36,100,'2021-11-20 08:32:04','bhavesh','2021-11-20 08:32:04','bhavesh'),(3,1,'Cake Pop',24,135,'2021-11-20 08:32:04','bhavesh','2021-11-20 08:32:04','bhavesh'),(4,1,'Apple tart',60,150,'2021-11-20 08:32:04','bhavesh','2021-11-20 08:32:04','bhavesh'),(5,1,'Water',30,150,'2021-11-20 08:32:04','bhavesh','2021-11-20 08:32:04','bhavesh'),(6,2,'Shirt',0,200,'2021-11-20 08:32:04','bhavesh','2021-11-20 08:32:04','bhavesh'),(7,2,'Pants',0,300,'2021-11-20 08:32:04','bhavesh','2021-11-20 08:32:04','bhavesh'),(8,2,'Jacket',0,400,'2021-11-20 08:32:04','bhavesh','2021-11-20 08:32:04','bhavesh'),(9,3,'Toy',0,100,'2021-11-20 08:32:04','bhavesh','2021-11-20 08:32:04','bhavesh');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `total` bigint NOT NULL DEFAULT '0',
  `status` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_detail`
--

DROP TABLE IF EXISTS `order_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_detail` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `order_id` bigint NOT NULL,
  `product_id` bigint NOT NULL,
  `quantity` int DEFAULT '0',
  `total_price` bigint DEFAULT '0',
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id_order_order_detail_idx` (`order_id`),
  KEY `product_id_product_order_detail_idx` (`product_id`),
  CONSTRAINT `order_id_order_order_detail` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`),
  CONSTRAINT `product_id_product_order_detail` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_detail`
--

LOCK TABLES `order_detail` WRITE;
/*!40000 ALTER TABLE `order_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_detail` ENABLE KEYS */;
UNLOCK TABLES;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-20 17:46:44
