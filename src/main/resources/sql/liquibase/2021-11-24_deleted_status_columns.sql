--liquibase formatted sql
--changeset bhaveshkhatri:changes
-- splitStatements:true

ALTER TABLE `category`
ADD COLUMN `deleted_at` DATETIME NULL AFTER `modified_by`,
ADD COLUMN `deleted_by` VARCHAR(255) NULL AFTER `deleted_at`;

ALTER TABLE `order`
ADD COLUMN `deleted_at` DATETIME NULL AFTER `modified_by`,
ADD COLUMN `deleted_by` VARCHAR(255) NULL AFTER `deleted_at`;

ALTER TABLE `cart_product`
ADD COLUMN `deleted_at` DATETIME NULL AFTER `modified_by`,
ADD COLUMN `deleted_by` VARCHAR(255) NULL AFTER `deleted_at`;

ALTER TABLE `product`
ADD COLUMN `deleted_at` DATETIME NULL AFTER `modified_by`,
ADD COLUMN `deleted_by` VARCHAR(255) NULL AFTER `deleted_at`;
