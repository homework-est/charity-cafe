--liquibase formatted sql
--changeset bhaveshkhatri:changes
-- splitStatements:true

CREATE TABLE `application_error` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `error_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_estonian_ci DEFAULT NULL,
  `message` text CHARACTER SET utf8 COLLATE utf8_estonian_ci,
  `error` text CHARACTER SET utf8 COLLATE utf8_estonian_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_estonian_ci DEFAULT NULL,
  `last_modified_at` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_estonian_ci DEFAULT NULL,
  `deleted_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_estonian_ci DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=589 DEFAULT CHARSET=utf8 COLLATE=utf8_estonian_ci;

